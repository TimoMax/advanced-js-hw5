


fetch('https://ajax.test-danit.com/api/json/posts')
  .then(res => res.json())
  .then(posts => {
    fetch('https://ajax.test-danit.com/api/json/users')
      .then(res => res.json())
      .then(users => {
        posts.forEach(post => {
          const { name, email } = users.find(el => el.id === post.userId);
          const card = new Card(post.title, post.body, name, email, post.id);
          card.render();
        });
        addDeleteHolder();
      });
  });

class Card {
  constructor(title, body, name, email, postId) {
    this.title = title;
    this.body = body;
    this.name = name;
    this.email = email;
    this.postId = postId;
  }

  render() {
    const div = `
      <div class="cardContainer" data-postid="${this.postId}">
        <div>
          <h3 class="title">${this.title}</h3>
          <p class="body">${this.body}</p>
          <p><span class="firstName">${this.name}</span></p>
          <a href="mailto:${this.email}">${this.email}</a>
        </div>
        <button class="deleteBtn">delete</button>
      </div>
    `;
    const body = document.querySelector('body');
    body.insertAdjacentHTML('beforebegin', div);
  }

  remove() {
    fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
      method: 'DELETE'
    })
      .then(response => {
        if (response.ok) {
          this.removeCardElement();
        } else {
          console.error('Error deleting card:', response.statusText);
        }
      })
      .catch(error => {
        console.error('Error deleting card:', error);
      });
  }

  removeCardElement() {
    const cardElement = document.querySelector(`[data-postid="${this.postId}"]`);
    if (cardElement) {
      cardElement.remove();
    }
  }
}

function addDeleteHolder() {
  const deleteButtons = document.querySelectorAll('.deleteBtn');
  deleteButtons.forEach(button => {
    button.addEventListener('click', (el) => {
      el.preventDefault();
      const cardContainer = button.closest('.cardContainer');
      const postId = cardContainer.dataset.postid;
      const card = new Card('', '', '', '', postId);
      card.remove();
    });
  });
}



